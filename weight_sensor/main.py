import sqlite3
from sqlite3 import Error
import requests
import json
import time
import sys

EMULATE_HX711=False

referenceUnit = 694

if not EMULATE_HX711:
    import RPi.GPIO as GPIO
    from hx711 import HX711
else:
    from emulated_hx711 import HX711
GPIO.setwarnings(False)

def cleanAndExit():
    print("Cleaning...")

    if not EMULATE_HX711:
        GPIO.cleanup()
    print("Bye!")
    sys.exit()
 

# data base connection function
def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return conn
 

def execute_query(query):
    conn = sqlite3.connect(r"raspberrypi.db")
    c = conn.cursor()
    c.execute(query)
    conn.commit()


def select_query(query):
    conn = sqlite3.connect(r"raspberrypi.db")    
    c = conn.cursor()
    c.execute(query)
    return c.fetchall()
 

def get_scaling_factor(id=None):
    #data = requests.get('http://5be8f90d.ngrok.io/get_data/')
    #data = data.json()
    data = {
            'sensor_id': 1, 
            'product_assigned': 1, 
            'product_weight': 100, 
            'current_weight': 20, 
            'sensor_state': '0', 
            'calibration_factor': 694, 
            'data_pin': 5, 
            'sck_pin': 6, 
            'pi_id': 0, 
            'cell_no': 0
        }
    sensor_objects = {}
    #for i in data:               #data["data"]:
    hx = HX711(data['data_pin'],data['sck_pin'])
    hx.set_reading_format("MSB", "MSB")
    hx.set_reference_unit(data['calibration_factor'])
    hx.cell_no = data['cell_no']
    hx.reset()
    hx.tare()
    sensor_id = data['sensor_id']
    sensor_objects[sensor_id] = hx
    return sensor_objects



    
        

def set_scaling_factor(scaling_factor, sensor_id):
    query = 'UPDATE sensor_data SET calibration_factor=' + scaling_factor + 'WHERE id=' + sensor_id + ';'
    execute_query(query)


def get_weight(hx):
    while True:
        try:
        # These three lines are usefull to debug wether to use MSB or LSB in the reading formats
        # for the first parameter of "hx.set_reading_format("LSB", "MSB")".
        # Comment the two lines "val = hx.get_weight(5)" and "print val" and uncomment these three lines to see what it prints.
        
        # np_arr8_string = hx.get_np_arr8_string()
        # binary_string = hx.get_binary_string()
        # print binary_string + " " + np_arr8_string
        
        # Prints the weight. Comment if you're debbuging the MSB and LSB
            if hx.cell_no == 0:
                print('a')
                val = hx.get_weight_A()
            else:
                print('b')
                val = hx.get_weight_B()
            print(val)
            return val

        # To get weight from both channels (if you have load cells hooked up 
        # to both channel A and B), do something like this
        #val_A = hx.get_weight_A(5)
        #val_B = hx.get_weight_B(5)
        #print "A: %s  B: %s" % ( val_A, val_B )

            hx.power_down()
            hx.power_up()
            time.sleep(0.1)
            CleanAndExit()
        except (KeyboardInterrupt, SystemExit):
            cleanAndExit()


def get_current_weight(id=None):
    if id:
        query = "select current_weight from sensor_data where id=" + str(id) + ";"
    else:
        query = "select current_weight from sensor_data;"
    return select_query(query)


def set_current_weight(id, current_weight, sensor_state):
    query = 'UPDATE sensor_data SET current_weight = ' + str(current_weight) + ', sensor_state= ' + str(sensor_state) + ' WHERE id =' + str(id) + ';'
    execute_query(query)
    

def send_data():
    temp = select_query("select * from sensor_data")
    output = []
    for i in temp:
        data = {}
        data['id'] = i[0]
        data['sensor_id'] = i[1]
        data['product_assign'] = i[2]
        data['product_weight'] = i[3]
        data['sensor_state'] = i[4]
        data['calibration_factor'] = i[5]
        data['current_weight'] = i[6]
        data['dt_pin'] = i[7]
        data['sck_pin'] = i[8]
        data['pi_id'] = i[9]
        data['cell_no'] = i[10]
        output.append(data)
    print(output)
    response_data = {
        "data" : output,
    }
    response_data = json.dumps(response_data)
    resp = requests.post('http://30ea6fc7.ngrok.io/get_data/', json=response_data)



def monitor_for_change(weight_sensor):
    offset = 5
    current_weight = {}
    for sensor_id, hx in weight_sensor.items():
        current_weight[sensor_id] = get_weight(hx)
    print(current_weight)
    for sensor_id, hx in weight_sensor.items():
        weight = get_weight(hx)
        if current_weight[sensor_id] < weight - offset:
            weight - current[sensor_id]
        if current_weight[sensor_id] > weight + offset:
            current_weight[sensor_id] - weight








def main():
    database = r"raspberrypi.db"
    #sensor_data table created if not existing
    query_create_table = """ CREATE TABLE IF NOT EXISTS sensor_data
                                    (
                                    id int not null primary key,
                                    sensor_id int,
                                    product_assigned varchar(255),
                                    product_weight int,
                                    sensor_state varchar(1),
                                    calibration_factor int,
                                    current_weight int,
                                    dt_pin int,
                                    sck_pin int,
                                    pi_id int,
                                    cell_no varchar(1)
                                    );
                                 """
    
    get_calibration = "select calibration_factor from sensor_data;"
    # create a database connection
    conn = create_connection(database)
 
    # create tables
    if conn is not None:
        # create sensor_data table
        #execute_query(query_create_table)
        pass
        #temp =  select_query(get_calibration)
        #for i in temp:
        #    print(i)
        #send_data()
        sensor_objects = get_scaling_factor()
        print('calibration done, now put weight')
        time.sleep(5)
        #get_weight(sensor_objects[1])
        #get_current_weight()
        #set_current_weight(21, 21000, 0)
        #send_data()
        monitor_for_change(sensor_objects)
 
    else:
        print("Error! cannot create the database connection.")


if __name__ == '__main__':
    main()
